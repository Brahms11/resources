#!/bin/bash

convert -background transparent -density 800 -resize 64 logo/logo-color.svg logo/logo-color-64.png
convert -background transparent -density 800 -resize 128 logo/logo-color.svg logo/logo-color-128.png
convert -background transparent -density 800 -resize 256 logo/logo-color.svg logo/logo-color-256.png
convert -background transparent -density 800 -resize 512 logo/logo-color.svg logo/logo-color-512.png
convert -background transparent -density 800 -resize 1024 logo/logo-color.svg logo/logo-color-1024.png

convert -background transparent -density 800 -resize 64 logo/logo-black.svg logo/logo-black-64.png
convert -background transparent -density 800 -resize 128 logo/logo-black.svg logo/logo-black-128.png
convert -background transparent -density 800 -resize 256 logo/logo-black.svg logo/logo-black-256.png
convert -background transparent -density 800 -resize 512 logo/logo-black.svg logo/logo-black-512.png
convert -background transparent -density 800 -resize 1024 logo/logo-black.svg logo/logo-black-1024.png
